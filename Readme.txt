#Instalacion y Configuracion Postgresql 11.14 Docker
docker run -p 5432:5432 --name postgres1114 -e root 
docker run --name some-postgres -v /home/ec2-user/dbbackup -e POSTGRES_PASSWORD=root -d postgres:11.14

#Puerto interno contenedor: Puerto de anfitrion
-v todas las bases de datos van a estar respaldadas /home/ec2-user/dbbackup
-d se ejecutara en modo demonio
ID: 09812fc52f5092ada0bae947ecb2a3cb09aead0e63312beec32e8ce6ad751b7b

# Revision ejecucion contenedor postgres:11.14 / some-postgres
docker ps

# Accedemos a postgres para realizar comandos SQL (Creacion de BD y Tablas)
docker exec -it some-postgres psql -U postgres

#Configuracion cron ejecucion pIngestaDataCron.py 3am
% crontab –e
00 03 * * * /usr/local/bin/python3 /home/ec2-user/Mutt Project/pIngestaDataCron.py
/
Ejemplo
2021-11-24 03:00:00 UTC
2021-11-25 03:00:00 UTC
2021-11-26 03:00:00 UTC
2021-11-27 03:00:00 UTC
2021-11-28 03:00:00 UTC

# Script Creacion Base de datos y tablas
CREATE DATABASE coingecko;

CREATE TABLE HRT_MONEDA
( 
ID_MONEDA serial PRIMARY KEY,
TIPO varchar(5)  NOT NULL,
PRECIO numeric(12,6)  NOT NULL,
JSON_DATE varchar  NOT NULL,
FEC_REG date  NOT NULL
);

CREATE TABLE HRT_MONEDA_AGG
( 
ID_MONEDA_AGG serial PRIMARY KEY,
ID_MONEDA bigint  NOT NULL,
YEAR integer  NOT NULL,
MONTH integer  NOT NULL,
VAL_MAX numeric(12,6) NOT NULL,
VAL_MIN numeric(12,6) NOT NULL
);

#Visualizamos Tablas creadas con el comando \d