import requests
import datetime as dt
import pathlib
import json
import os

def getdata(vcoin,vdate):
    URL = "https://api.coingecko.com/api/v3/coins/"+vcoin+"/history?date="+vdate
    print("GET" + URL)
    r = requests.get(url = URL)
    data = r.json()
    with open('/home/ec2-user/Mutt Project/ingestFiles/coingecko_'+ vcoin + '_'+  vdate + '.json' , 'w') as f:
    #with open('C:/Users/Richard/Documents/Mutt Project/ingestFiles/coingecko_'+  vdate + '.json' , 'w') as f:
        json.dump(data, f, indent = 4)

def validateDate(fecha):
    while True:   
        try:
            dt.datetime.strptime(fecha, '%Y-%m-%d')
            print("Fecha válida")
            return True
        except ValueError:
            print("Fecha inválida")
            return False


vCoin = input("Indique la moneda (ethereum/bitcoin/cardano): ")
vDate = input(f"Indique fecha historica YYYY-MM-DD: ")

if vCoin == 'ethereum' or vCoin == 'bitcoin' or vCoin == 'cardano':
    validateDate(vDate)
    vDateIngest =  dt.datetime.strptime(vDate, '%Y-%m-%d').strftime('%d-%m-%Y')
    print("Se procede a extraer informacion...")
    getdata(vCoin,vDateIngest)
else:
   print("Debe ingresar un tipo de moneda valida para su extraccion")



