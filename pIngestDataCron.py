import requests
import datetime as dt
import pathlib
import json
import os

def getdata(vcoin,vdate):
    URL = "https://api.coingecko.com/api/v3/coins/"+vcoin+"/history?date="+vdate
    print("GET" + URL)
    r = requests.get(url = URL)
    data = r.json()
    with open('/home/ec2-user/Mutt Project/ingestFiles/coingecko_'+ vcoin + '_'+  vdate + '.json' , 'w') as f:
    #with open('C:/Users/Richard/Documents/Mutt Project/ingestFiles/coingecko_'+ vcoin + '_'+  vdate + '.json' , 'w') as f:
        json.dump(data, f, indent = 4)

def validateDate(fecha):
    while True:   
        try:
            dt.datetime.strptime(fecha, '%Y-%m-%d')
            print("Fecha válida")
            return True
        except ValueError:
            print("Fecha inválida")
            return False


vdate = dt.datetime.now()
processDateIni = vdate.strftime('%d-%m-%Y')
print(processDateIni)
vcoin = 'ethereum'
getdata(vcoin,processDateIni)
vcoin2 = 'bitcoin'
getdata(vcoin2,processDateIni)
vcoin3 = 'cardano'
getdata(vcoin3,processDateIni)


